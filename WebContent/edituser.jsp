<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${editUser.account}の編集</title>
        <link href="css/edituser.css" rel="stylesheet" type="text/css">
        <link href="./css/errorMessages.css" rel="stylesheet" type="text/css">

    </head>

    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>


        <div class="left">
	        <a href="home" class="button">ホーム</a><br />
	        <a href="alluser" class="button">ユーザー管理へ戻る</a><br />
	        <a href="logout" class="button">ログアウト</a>
        </div>

		<div class="right">
            <form action="edit" method="post">
                <input name="id" value="${editUser.id}" id="id" type="hidden"/>

                <label for="account" class="font_size">アカウント名　(半角英数字, 6-20文字)</label>
                <input name="account" value="${editUser.account}" class="box"/><br />

                <label for="password" class="font_size">パスワード　(半角英数字, 6-20文字)</label>
                <input name="password" type="password" id="password" class="box"/> <br />

 				<label for="check_password" class="font_size">確認用パスワード</label>
                <input name="check_password" type="password" id="check_password" class="box"/> <br />


                <label for="name" class="font_size">名前　(10文字以内)</label>
                <input name="name" value="${editUser.name}" id="name" class="box"/> <br />

				<c:if test="${loginUser.id != editUser.id}">
	                <label for="branch_id" class="font_size">支店</label>
	                <select name="branch_id" id="branches" class="box">
		                <option value="" class="font_size">選択してください</option>


						<c:forEach var="branch" items="${branches}" varStatus="status">
							<c:if test="${branch.id == editUser.branch_id}">
								<option value="${branch.id}" selected>${branch.name} </option>
							</c:if>
							<c:if test="${branch.id != editUser.branch_id}">
								<option value="${branch.id}">${branch.name} </option>
							</c:if>
						</c:forEach>
	                </select>
	                <br />

	                <label for="department_id" class="font_size">部署・役職</label>
	                <select name="department_id" id="departments" class="box">
		                <option value="" class="font_size">選択してください</option>
		                <c:forEach var="department" items="${departments}" varStatus="status">
		                	<c:if test="${department.id == editUser.department_id}">
								<option value="${department.id}" selected>${department.name} </option>
							</c:if>
							<c:if test="${department.id != editUser.department_id}">
								<option value="${department.id}">${department.name} </option>
							</c:if>
						</c:forEach>
	                </select>
	                <br />
				</c:if>

                <input type="submit" value="編集" class="regist"/> <br />
            </form>
			<c:remove var="keepUser" scope="session"/>
        </div>
        </div>
    </body>
</html>