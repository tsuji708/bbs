<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー管理</title>
        <link href="./css/alluser.css" rel="stylesheet" type="text/css">
        <link href="./css/errorMessages.css" rel="stylesheet" type="text/css">

    </head>

    <body>
    <div class="header">
    	<c:if test="${ not empty loginUser }">

	        	<c:if test="${ not empty errorMessages }">
	                <div class="errorMessages">
	                    <ul>
	                        <c:forEach items="${errorMessages}" var="message">
	                            <li><c:out value="${message}" />
	                        </c:forEach>
	                    </ul>
	                </div>
	                <c:remove var="errorMessages" scope="session" />
	            </c:if>

	    <div class="left">
        	<a href="home" class="button">ホーム</a><br/>
        	<a href="regist" class="button">新規登録</a><br/>
        	<a href="logout" class="button">ログアウト</a>
		</div>

		<div class="right">
		    <div class="users">
				<table>

			    	<tr>
			    	<th>アカウント名</th>
			    	<th>ユーザー名</th>
			    	<th>支店ID</th>
			    	<th>部署ID</th>
			    	<th>編集</th>
			    	<th>切り替え</th>
			    	</tr>

		    		<c:forEach items="${users}" var="user">
					<tr>
			            <td class="account"><c:out value="${user.account}" /></td>
			            <td class="name"><c:out value="${user.name}" /></td>
			            <td class="branch_id"><c:out value="${user.branch_name}" /></td>
			            <td class="department_id"><c:out value="${user.department_name}" /></td>
						<td>
						<form action="edit" method="get">
						<input type="hidden" name="id" value="${user.id}" />
						<input type="submit"  value="編集" class="edit_button"/>
						</form>
			            </td>

			            <td>
			            <c:if test="${user.id == loginUser.id}">
			            	<label for="loginNow">ログイン中</label>
			            </c:if>

			            <script>

    					function clickEvent(processname) {
        					return confirm(processname + 'しますか？');
    					}
						</script>

						<c:if test="${user.id != loginUser.id}">
							<form action="account_management" method="post">
						    	<input type="hidden" name="user_id" value="${user.id}" />
								<c:if test="${user.is_stopped == 0}">
									<input type="hidden" name="is_stopped" value="1" />
									<input type="submit" name="off"  value="停止" onclick="return clickEvent('停止');"  class="stopp_button"/>
								</c:if>

								<c:if test="${user.is_stopped == 1}">
								<input type="hidden" name="is_stopped" value="0" />
								<input type="submit" name="on"  value="再開" onclick="return clickEvent('再開');" class="reconnect_button"/>
								</c:if>
							</form>
						</c:if>
						</td>
					</tr>
		    		</c:forEach>

		    	</table>
			</div>
		</div>
    	</c:if>
	</div>
    </body>
</html>

