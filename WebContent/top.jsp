<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>掲示板</title>
        <link href="./css/top.css" rel="stylesheet" type="text/css">
        <link href="./css/errorMessages.css" rel="stylesheet" type="text/css">
    </head>

    <body>
    <div class="header">
    </div>

    <c:if test="${ not empty errorMessages }">
	                <div class="errorMessages">
	                    <ul>
	                        <c:forEach items="${errorMessages}" var="message">
	                            <li><c:out value="${message}" />
	                        </c:forEach>
	                    </ul>
	                </div>
	                <c:remove var="errorMessages" scope="session" />
	            </c:if>

    <div class="left">
    	<div>
	    	<c:if test="${ empty loginUser }">
	        		<a href="login" class="button">ログイン</a>
	    	</c:if>
    	</div>


    	<c:if test="${ not empty loginUser }">
	        <a href="newMessage"  class="button">新規投稿</a> <br/>
		        <c:if test="${ loginUser.branch_id ==1 && loginUser.department_id ==1 }">
		        	<a href="alluser"  class="button">ユーザー設定</a>
		        </c:if><br/>
			<a href="logout"  class="button">ログアウト</a><br/>
	    </c:if>


		<div class="search">
			<form action="./" method="get">

					<div class="category_menue">カテゴリ</div>
					<input type="text" name="category" class="category_box"><br/>
					<div class="date_menue">日付</div>
					<input type="date" name="from" class="date1"><br/>
					<input type="date" name="to" class="date2"><br/>
					<input type="submit" value="検索" class="search_button">
			</form>
		</div>
	</div>

	<div class="right">
		    <div class="messages">
		    	<c:forEach items="${messages}" var="message">
		            <div class="mainmessage">

		                <div class="title">
		                <span class="box-title">Title</span>
		                <p>
		                <c:out value="${message.title}" />
		                </p>
		               	</div>

		                <div class="main">
		                <c:forEach var="s" items="${fn:split(message.message, '
		                ')}">
							<c:out value="${s}" /> <br/>
						</c:forEach>
						</div>

						<span class="category">カテゴリー; <c:out value="${message.category}" /> </span>
		                <span class="name">投稿者;<c:out value="${message.name}" />
		                <fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></span>

		                <c:if test= "${loginUser.id == message.userId}">
						<form action="deletemessage" method="post" class="message_delete"><br />
							<div>
				                <input type="hidden" name="message_id" value="${message.id}" /> <br />
				                <input type="submit" value="投稿削除" class="message-delete"/> <br />
							</div>
						</form>
						</c:if>

					</div>






					<c:forEach items="${comments}" var="comment">
						<c:if test= "${message.id == comment.message_id}">
			            	<div class="comment">
			            	<div class="comment_in">
			            		<div class="form">
			            		<form action="deletecomment" method="post">
			            		<span class="comment_span1">
								<c:forEach var="co" items="${fn:split(comment.comment, '
			                	')}">
								<c:out value="${co}"  /> <br/>
								</c:forEach>
								<span class="comment_name"><c:out value="${comment.name}　　" /></span>
								</span >
					            <c:if test= "${loginUser.id == comment.userId}">
											<span class="comment_span2">
												<input type="hidden" name="comment_id" value="${comment.id}" />
												<input type="submit" value="削除" class="comment_delete"/>
											</span>

								</c:if>
								</form>
								</div>
							</div>
							</div>
			            </c:if>
					</c:forEach>
					<br />

					<div class="main-contents">
						<c:if test="${ not empty commentErrorMessages }">
							<c:if test= "${message.id == keepComment.messageId}">
								<div class="errorMessages">
									<ul>
										<c:forEach items="${commentErrorMessages}" var="errmessage">
											<li><c:out value="${errmessage}" />
										</c:forEach>
									</ul>
								</div>
								<c:remove var="commentErrorMessages" scope="session"/>
							</c:if>
						</c:if>

						<div class="comment_input">
							<form action="newComment" method="post"><br />
								＜コメント投稿＞　(500字以内)
								<div class="comment-box">
									<textarea name="comment" cols="50" rows="2" class="comment_box"><c:if test= "${message.id == keepComment.messageId}">${keepComment.comment}</c:if></textarea>
									<input type="hidden" name="message_id" value="${message.id}" />
									<input type="submit" value="コメント" class="comment-button"/>
								</div>
							</form>
						</div>
					</div>
		    	</c:forEach>
		    	<c:remove var="keepComment" scope="session"/>
			</div>
	</div>
    </body>
</html>

