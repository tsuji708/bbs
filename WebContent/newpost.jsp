<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>新規投稿</title>
	<link href="./css/newpost.css" rel="stylesheet" type="text/css">
        <link href="./css/errorMessages.css" rel="stylesheet" type="text/css">

	</head>

	<body>
		<div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

		<div class="left">
			<a href="home" class="button">ホーム</a><br/>
			<a href="logout" class="button">ログアウト</a>
		</div>

		<div class="right">
	 		<form action="newMessage" method="post">
	        	<div class="font_size">タイトル　(30字以内)</div>
				<input type="text" name="title" value="${keepMessage.title}" class="box"/>


	            <div class="font_size">本文　(1000字以内)</div>
	            <textarea name="message" cols="100" rows="10" class="text_box">${keepMessage.message}</textarea><br />

	            <div class="font_size">カテゴリ　(10字以内)</div>
	      		<input type="text" name="category" value="${keepMessage.category}" class="box"/>
	            <input type="submit" value="投稿" class="regist">
	            <c:remove var="keepMessage" scope="session"/>
	        </form>
	    </div>
		</div>
	</body>
</html>





