package bbs.service;

import static bbs.util.CloseableUtil.*;
import static bbs.util.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import bbs.beans.User;
import bbs.dao.UserDao;
import bbs.util.CipherUtil;

public class UserService {

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    public User getEditUser(int edit_id) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            User ret = userDao.getEditUser(connection, edit_id);

            commit(connection);
            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    public void update(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            if(StringUtils.isEmpty(user.getPassword()) != true){
            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);
            }

            UserDao userDao = new UserDao();
            userDao.update(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    	public void accountmanagement(int user_id, int is_stopped) {

            Connection connection = null;
            try {
                connection = getConnection();

                UserDao userDao = new UserDao();
                userDao.update(connection, user_id, is_stopped);

                commit(connection);
            } catch (RuntimeException e) {
                rollback(connection);
                throw e;
            } catch (Error e) {
                rollback(connection);
                throw e;
            } finally {
                close(connection);
            }
        }


    	 public List<User> getAllUser() {

    	        Connection connection = null;
    	        try {
    	            connection = getConnection();

    	            UserDao userDao = new UserDao();
    	            List<User> ret = userDao.getAllUserList(connection);

    	            commit(connection);

    	            return ret;
    	        } catch (RuntimeException e) {
    	            rollback(connection);
    	            throw e;
    	        } catch (Error e) {
    	            rollback(connection);
    	            throw e;
    	        } finally {
    	            close(connection);
    	        }
    	    }


    	 public User getExistsUser(String account) {

    	        Connection connection = null;
    	        try {
    	            connection = getConnection();

    	            UserDao userDao = new UserDao();
    	            User ret = userDao.getExistsUser(connection, account);

    	            commit(connection);

    	            return ret;
    	        } catch (RuntimeException e) {
    	            rollback(connection);
    	            throw e;
    	        } catch (Error e) {
    	            rollback(connection);
    	            throw e;
    	        } finally {
    	            close(connection);
    	        }
    	    }
}
