package bbs.service;

import static bbs.util.CloseableUtil.*;
import static bbs.util.DBUtil.*;

import java.sql.Connection;

import bbs.dao.MessageDao;

public class DeleteMessageService {

	public void deletemessage(int deletemessage_id) {

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.delete(connection, deletemessage_id);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}
