package bbs.service;

import static bbs.util.CloseableUtil.*;
import static bbs.util.DBUtil.*;

import java.sql.Connection;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Message;
import bbs.beans.UserMessage;
import bbs.dao.MessageDao;
import bbs.dao.UserMessageDao;

public class MessageService {

    public void register(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.insert(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    private static final int LIMIT_NUM = 1000;

    public List<UserMessage> getMessage(String category, String from, String to) {

        Connection connection = null;
        try {
            connection = getConnection();

            if(StringUtils.isEmpty(from)){
            	from = "2017-04-01";
            }

            if(StringUtils.isEmpty(to)){
            	Calendar calendar=Calendar.getInstance();
            	int year=calendar.get(Calendar.YEAR);
            	int month=calendar.get(Calendar.MONTH)+1;
            	int day=calendar.get(Calendar.DATE);
            	to = year + "-" + month + "-" + day;
            }

            UserMessageDao messageDao = new UserMessageDao();
            List<UserMessage> ret = messageDao.getUserMessages(connection, LIMIT_NUM, category, from, to);

            commit(connection);
            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }



}