package bbs.dao;

import static bbs.util.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bbs.beans.UserComment;
import bbs.exception.SQLRuntimeException;

public class UserCommentDao {

    public List<UserComment> getUserComment(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.comment as comment, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("comments.message_id as message_id, ");
            sql.append("users.account as account, ");
            sql.append("users.name as name, ");
            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


    private List<UserComment> toUserCommentList(ResultSet rs)
            throws SQLException {

        List<UserComment> ret = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
                String account = rs.getString("account");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int message_id =rs.getInt("message_id");
                int userId = rs.getInt("user_id");
                String comment = rs.getString("comment");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserComment usercomment = new UserComment();
                usercomment.setAccount(account);
                usercomment.setName(name);
                usercomment.setId(id);
                usercomment.setMessage_id(message_id);
                usercomment.setUserId(userId);
                usercomment.setComment(comment);
                usercomment.setCreatedDate(createdDate);

                ret.add(usercomment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}
