package bbs.dao;

import static bbs.util.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import bbs.beans.Comment;
import bbs.exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append("user_id");
            sql.append(", comment");
            sql.append(", message_id");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", CURRENT_TIMESTAMP");
            sql.append(", CURRENT_TIMESTAMP");
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, comment.getUserId());
            ps.setString(2, comment.getComment());
            ps.setInt(3, comment.getMessageId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


    public void delete(Connection connection,int deletecomment_id) {

    	PreparedStatement ps = null;
         try {
             StringBuilder sql = new StringBuilder();
             sql.append(" DELETE FROM comments WHERE id = ? ");

             ps = connection.prepareStatement(sql.toString());

             ps.setInt(1, deletecomment_id);

             ps.executeUpdate();
         } catch (SQLException e) {
             throw new SQLRuntimeException(e);
         } finally {
             close(ps);
         }

    }
}