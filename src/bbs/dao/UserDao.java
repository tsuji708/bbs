package bbs.dao;

import static bbs.util.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import bbs.beans.User;
import bbs.exception.NoRowsUpdatedRuntimeException;
import bbs.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("account");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", department_id");
            sql.append(", is_stopped");
            sql.append(") VALUES (");
            sql.append("?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", 0");
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setString(4, user.getBranch_id());
            ps.setString(5, user.getDepartment_id());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


    public User getUser(Connection connection, String account,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE account = ? AND password = ? AND is_stopped = 0" ;

            ps = connection.prepareStatement(sql);
            ps.setString(1, account);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String account = rs.getString("account");
                String password = rs.getString("password");
                String name = rs.getString("name");
                String branch_id = rs.getString("branch_id");
                String department_id = rs.getString("department_id");
                String is_stopped = rs.getString("is_stopped");

                User user = new User();
                user.setId(id);
                user.setAccount(account);
                user.setPassword(password);
                user.setName(name);
                user.setBranch_id(branch_id);
                user.setDepartment_id(department_id);
                user.setIs_stopped(is_stopped);


                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }


    public User getEditUser(Connection connection, int edit_id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, edit_id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append(" name = ?");
            sql.append(", account = ?");
            if (StringUtils.isEmpty(user.getPassword()) != true) {
            sql.append(", password = ?");
            }
            sql.append(", branch_id = ?");
            sql.append(", department_id = ?");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getName());
            ps.setString(2, user.getAccount());

            if (StringUtils.isEmpty(user.getPassword()) != true) {
	            ps.setString(3, user.getPassword());
	            ps.setString(4, user.getBranch_id());
	            ps.setString(5, user.getDepartment_id());
	            ps.setInt(6, user.getId());
            } else {
            	ps.setString(3, user.getBranch_id());
 	            ps.setString(4, user.getDepartment_id());
 	            ps.setInt(5, user.getId());
            }

            int count = ps.executeUpdate();

            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


    public void update(Connection connection,  int user_id, int is_stopped) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append(" is_stopped = ?");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, is_stopped);
            ps.setInt(2, user_id);

            int count = ps.executeUpdate();

            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


    public List <User> getAllUserList(Connection connection) {

        PreparedStatement ps = null;
        try {
        	 StringBuilder sql = new StringBuilder();

            sql.append("SELECT ");
            sql.append("users.id as id, ");
            sql.append("users.account as account, ");
            sql.append("users.password as password, ");
            sql.append("users.name as name, ");
            sql.append("users.branch_id as branch_id, ");
            sql.append("users.department_id as department_id, ");
            sql.append("branches.name as branch_name, ");
            sql.append("departments.name as department_name, ");
            sql.append("users.is_stopped as is_stopped ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branches ");
            sql.append("ON users.branch_id = branches.id ");
            sql.append("INNER JOIN departments ");
            sql.append("ON users.department_id = departments.id ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<User> alluserList = toAllUserList(rs);

            return alluserList;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toAllUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String account = rs.getString("account");
                String password = rs.getString("password");
                String name = rs.getString("name");
                String branch_name = rs.getString("branch_name");
                String department_name = rs.getString("department_name");
                String is_stopped = rs.getString("is_stopped");

                User user = new User();
                user.setId(id);
                user.setAccount(account);
                user.setPassword(password);
                user.setName(name);
                user.setBranch_name(branch_name);
                user.setDepartment_name(department_name);
                user.setIs_stopped(is_stopped);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }


    public User getExistsUser(Connection connection, String account) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE account = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, account);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);

            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}