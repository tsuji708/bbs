package bbs.dao;

import static bbs.util.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import bbs.beans.UserMessage;
import bbs.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection, int num, String category, String from, String to) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.title as title, ");
            sql.append("messages.message as message, ");
            sql.append("messages.category as category, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("users.account as account, ");
            sql.append("users.name as name, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("WHERE created_date between ? and ? ");
            if(!StringUtils.isEmpty(category)){
            sql.append("and category LIKE ? ");
            }
            sql.append("ORDER BY created_date DESC limit " + num);

            System.out.println(sql.toString());

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, from + " 00:00:00");
            ps.setString(2, to + " 23:59:59");
            if(!StringUtils.isEmpty(category)){
            ps.setString(3, "%" + category + "%");
            }

            System.out.println(ps);
            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                String account = rs.getString("account");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                String title = rs.getString("title");
                String message = rs.getString("message");
                String category = rs.getString("category");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserMessage userMessage = new UserMessage();
                userMessage.setAccount(account);
                userMessage.setName(name);
                userMessage.setId(id);
                userMessage.setUserId(userId);
                userMessage.setTitle(title);
                userMessage.setMessage(message);
                userMessage.setCategory(category);
                userMessage.setCreated_date(createdDate);

                ret.add(userMessage);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}