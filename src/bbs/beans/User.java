package bbs.beans;

import java.io.Serializable;

public class User implements Serializable {
    private static final long serialVersionUID = 1L;


    private int id;
    private String account;
    private String password;
    private String name;
    private String branch_id;
    private String department_id;
	private String is_stopped;

	private String branch_name;
    private String department_name;


    public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBranch_id() {
		return branch_id;
	}
	public void setBranch_id(String branch_id) {
		this.branch_id = branch_id;
	}
	public String getDepartment_id() {
		return department_id;
	}
	public void setDepartment_id(String department_id) {
		this.department_id = department_id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	 public String getIs_stopped() {
			return is_stopped;
		}
	public void setIs_stopped(String is_stopped) {
			this.is_stopped = is_stopped;
	}


	public String getBranch_name() {
		return branch_name;
	}
	public void setBranch_name(String branch_name) {
		this.branch_name = branch_name;
	}
	public String getDepartment_name() {
		return department_name;
	}
	public void setDepartment_name(String department_name) {
		this.department_name = department_name;
	}
	public Object getParameter(String string) {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

}