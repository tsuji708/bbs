package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Branches;
import bbs.beans.Departments;
import bbs.beans.User;
import bbs.service.BranchService;
import bbs.service.DepartmentService;
import bbs.service.UserService;

@WebServlet(urlPatterns = { "/regist" })
public class RegistServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        User loginUser = (User) session.getAttribute("loginUser");

        List<Branches> branches = new BranchService().getBranches();
        List<Departments> departments = new DepartmentService().getDepartments();

        session.setAttribute("branches", branches);
        session.setAttribute("departments", departments);

        request.getRequestDispatcher("regist.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();

        User keepUser = KeepUser(request);

        if (isValid(request, messages) == true) {

            User user = new User();
            user.setAccount(request.getParameter("account"));
            user.setPassword(request.getParameter("password"));
            user.setName(request.getParameter("name"));
            user.setBranch_id(request.getParameter("branch_id"));
            user.setDepartment_id(request.getParameter("department_id"));

            new UserService().register(user);

            response.sendRedirect("./alluser");
        } else {
            session.setAttribute("errorMessages", messages);
    		session.setAttribute("keepUser", keepUser);

    		request.getRequestDispatcher("regist.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String account = request.getParameter("account");
        String password = request.getParameter("password");
        String check_password = request.getParameter("check_password");
        String name = request.getParameter("name");
        String branch_id = request.getParameter("branch_id");
        String department_id = request.getParameter("department_id");

        if (StringUtils.isEmpty(account) == true) {
            messages.add("アカウント名を入力してください");
        } else {

        	User existsUser = new UserService().getExistsUser(request.getParameter("account"));

        	if (existsUser != null){
	        	messages.add("アカウント名がすでに存在します");
	        } else {
	        	if(account.length() <6 || 20< account.length()){
	        		messages.add("アカウント名を6文字以上,20文字以下で入力してください");
	        	}
	        	if(!account.matches("^[0-9a-zA-Z]*$")){
		        	messages.add("アカウント名を半角英数字で入力してください");
		        }
	        }
        }

        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        } else {
	        if (password.length() <6 ||20 < password.length()) {
	            messages.add("パスワードを6文字以上,20文字以下で入力してください");
	        }
	        if(!password.matches("^[0-9a-zA-Z]{6,20}$")){
	        	messages.add("パスワードを半角英数字で入力してください");
	        }
        }

        if (!check_password.equals(password)){
        	messages.add("パスワードが一致しません");
        }

        if (StringUtils.isEmpty(name) == true) {
            messages.add("名前を入力してください");
        }
        if (10 < name.length()) {
            messages.add("名前を10文字以下で入力してください");
        }

        if (branch_id == ""){
        	messages.add("支店を選択してください");
        }

        if (department_id == ""){
        	messages.add("部署を選択してください");
        }

        if (branch_id.equals("1") && department_id.equals("3")){
        	messages.add("支店と部署の組み合わせが間違っています");
        }

        if (branch_id.equals("1") && department_id.equals("4")){
        	messages.add("支店と部署の組み合わせが間違っています");
        }

        if (branch_id.equals("2") && department_id.equals("1")){
        	messages.add("支店と部署の組み合わせが間違っています");
        }

        if (branch_id.equals("2") && department_id.equals("2")){
        	messages.add("支店と部署の組み合わせが間違っています");
        }

        if (branch_id.equals("3") && department_id.equals("1")){
        	messages.add("支店と部署の組み合わせが間違っています");
        }

        if (branch_id.equals("3") && department_id.equals("2")){
        	messages.add("支店と部署の組み合わせが間違っています");
        }

        if (branch_id.equals("4") && department_id.equals("1")){
        	messages.add("支店と部署の組み合わせが間違っています");
        }

        if (branch_id.equals("4") && department_id.equals("2")){
        	messages.add("支店と部署の組み合わせが間違っています");
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    private User KeepUser(HttpServletRequest request)
            throws IOException, ServletException {

        User keepUser = new User();

        keepUser.setAccount(request.getParameter("account"));
        keepUser.setPassword(request.getParameter("password"));
        keepUser.setName(request.getParameter("name"));
        keepUser.setBranch_id(request.getParameter("branch_id"));
        keepUser.setDepartment_id(request.getParameter("department_id"));
        return keepUser;
    }

}
