
package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Branches;
import bbs.beans.Departments;
import bbs.beans.User;
import bbs.exception.NoRowsUpdatedRuntimeException;
import bbs.service.BranchService;
import bbs.service.DepartmentService;
import bbs.service.UserService;



@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        User loginUser = (User) session.getAttribute("loginUser");

        String id = request.getParameter("id");
        if (!id.matches("^[0-9]+$")){
    		List<String> messages = new ArrayList<String>();
        	messages.add("ユーザーが存在しません");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("alluser");
			return;
        }

    	User editUser = new UserService().getEditUser(Integer.parseInt(id));

    	if (editUser == null) {
    		List<String> messages = new ArrayList<String>();
        	messages.add("ユーザーが存在しません");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("alluser");
			return;
    	}

        request.setAttribute("editUser", editUser);

        List<Branches> branches = new BranchService().getBranches();
        List<Departments> departments = new DepartmentService().getDepartments();
        session.setAttribute("branches", branches);
        session.setAttribute("departments", departments);

        request.getRequestDispatcher("edituser.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();

        User editUser = getEditUser(request);
        User keepUser = KeepUser(request);
        User loginUser = (User) session.getAttribute("loginUser");

        if (isValid(request, messages) == true) {

            try {
                new UserService().update(editUser);
            } catch (NoRowsUpdatedRuntimeException e) {
                session.setAttribute("errorMessages", messages);
                request.setAttribute("editUser", editUser);
                request.getRequestDispatcher("alluser.jsp").forward(request, response);
                return;
            }

            if (editUser == loginUser){
            session.setAttribute("loginUser", editUser);
            }

            response.sendRedirect("alluser");
        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("editUser", editUser);
    		session.setAttribute("keepUser", keepUser);
    		request.getRequestDispatcher("edituser.jsp").forward(request, response);
    		}
    }

    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {

        User editUser = new User();
        HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("loginUser");
        String password = request.getParameter("password");


        editUser.setId(Integer.parseInt(request.getParameter("id")));
        editUser.setName(request.getParameter("name"));
        editUser.setAccount(request.getParameter("account"));

        if (StringUtils.isEmpty(password) != true){
        editUser.setPassword(request.getParameter("password"));
        }

        if (editUser.getId() != loginUser.getId()){
        editUser.setBranch_id(request.getParameter("branch_id"));
        editUser.setDepartment_id(request.getParameter("department_id"));
        }
        if(editUser.getId() == loginUser.getId()){
        	editUser.setBranch_id(loginUser.getBranch_id());
            editUser.setDepartment_id(loginUser.getDepartment_id());
        }

        return editUser;
    }


    private boolean isValid(HttpServletRequest request, List<String> messages) {


    	HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("loginUser");
        User editUser = new UserService().getEditUser(Integer.parseInt(request.getParameter("id")));

    	String account = request.getParameter("account");
        String password = request.getParameter("password");
        String check_password = request.getParameter("check_password");
        String name = request.getParameter("name");
        String branch_id = request.getParameter("branch_id");
        String department_id = request.getParameter("department_id");

        System.out.println(!account.matches("^[0-9a-zA-Z]$"));
        if (StringUtils.isEmpty(account) == true) {
        	System.out.println("a");
            messages.add("アカウント名を入力してください");
        } else {
        	System.out.println("b");
	        if (!editUser.getAccount().equals(account) ){
	        	System.out.println("c");
	        	User existsUser = new UserService().getExistsUser(request.getParameter("account"));
	        	if(existsUser != null){
	        		System.out.println("d");
	        	messages.add("アカウントがすでに存在します");
	        	}
	        }
	        if (!editUser.getAccount().equals(account) ){
	        	System.out.println("e");
	        	if(account.length() <6 || 20< account.length()){
	        		System.out.println("f");
	        		messages.add("アカウント名を6文字以上,20文字以下で入力してください");
	        	}
	        	System.out.println("g");
	        	if(!account.matches("^[0-9a-zA-Z]$")){
	        		System.out.println("h");
		        	messages.add("アカウント名を半角英数字で入力してください");
		        }
	        }
	        System.out.println("i");
        }
        System.out.println("j");

        if (StringUtils.isEmpty(password) != true || StringUtils.isEmpty(check_password) != true){

	        if (password.length() <6 || 20< password.length()) {
	            messages.add("パスワードを6文字以上,20文字以下で入力してください");
	        }
	        if(!password.matches("^[0-9a-zA-Z]*$")) {
	        	messages.add("パスワードを半角英数字で入力してください");
	        }

	        if (check_password.length() <6 || 20< check_password.length()) {
	            messages.add("確認パスワードを6文字以上,20文字以下で入力してください");
	        } else if(!check_password.matches("^[0-9a-zA-Z]{6,20}")) {
	        	messages.add("確認パスワードを半角英数字で入力してください");
	        } else if (!check_password.equals(password)){
	        	messages.add("パスワードが一致しません");
	        }

        }

        if (loginUser.getId() != editUser.getId()){

	        if (StringUtils.isEmpty(name) == true) {
	            messages.add("名前を入力してください");
	        }

	        if (10 < name.length()) {
	            messages.add("名前を10文字以下で入力してください");
	        }

	        if (branch_id == ""){
	        	messages.add("支店を選択してください");
	        }

	        if (department_id == ""){
	        	messages.add("部署を選択してください");
	        }

	        if (branch_id.equals("1") && department_id.equals("3")){
	        	messages.add("支店と部署の組み合わせが間違っています");
	        }

	        if (branch_id.equals("1") && department_id.equals("4")){
	        	messages.add("支店と部署の組み合わせが間違っています");
	        }

	        if (branch_id.equals("2") && department_id.equals("1")){
	        	messages.add("支店と部署の組み合わせが間違っています");
	        }

	        if (branch_id.equals("2") && department_id.equals("2")){
	        	messages.add("支店と部署の組み合わせが間違っています");
	        }

	        if (branch_id.equals("3") && department_id.equals("1")){
	        	messages.add("支店と部署の組み合わせが間違っています");
	        }

	        if (branch_id.equals("3") && department_id.equals("2")){
	        	messages.add("支店と部署の組み合わせが間違っています");
	        }

	        if (branch_id.equals("4") && department_id.equals("1")){
	        	messages.add("支店と部署の組み合わせが間違っています");
	        }

	        if (branch_id.equals("4") && department_id.equals("2")){
	        	messages.add("支店と部署の組み合わせが間違っています");
	        }

        }


        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }


    private User KeepUser(HttpServletRequest request)
            throws IOException, ServletException {

        User keepUser = new User();

        keepUser.setAccount(request.getParameter("account"));
        keepUser.setPassword(request.getParameter("password"));
        keepUser.setName(request.getParameter("name"));
        keepUser.setBranch_id(request.getParameter("branch_id"));
        keepUser.setDepartment_id(request.getParameter("department_id"));
        return keepUser;
    }

}