package bbs.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bbs.beans.User;
import bbs.beans.UserComment;
import bbs.beans.UserMessage;
import bbs.service.CommentService;
import bbs.service.MessageService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	User user = (User) request.getSession().getAttribute("loginUser");

        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }

        String category = request.getParameter("category");
    	String from = request.getParameter("from");
        String to = request.getParameter("to");

        List<UserMessage> messages = new MessageService().getMessage(category, from, to);
        request.setAttribute("messages", messages);

        List<UserComment> comments = new CommentService().getComment();
        request.setAttribute("comments", comments);

        request.setAttribute("isShowMessageForm", isShowMessageForm);

    	request.getRequestDispatcher("/top.jsp").forward(request, response);

    }

}

