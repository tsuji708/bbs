package bbs.controller;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bbs.beans.User;
import bbs.service.DeleteCommentService;


	@WebServlet(urlPatterns = { "/deletecomment" })
	public class DeleteCommentServlet extends HttpServlet{
		 private static final long serialVersionUID = 1L;

		    @Override
		    protected void doPost(HttpServletRequest request,
		            HttpServletResponse response) throws IOException, ServletException {

		    	 HttpSession session = request.getSession();
		    	 User user = (User) session.getAttribute("loginUser");

		    	 int  deletecomment_id = (Integer.parseInt(request.getParameter("comment_id")));
		    	 new DeleteCommentService().deletecomment(deletecomment_id);

		         response.sendRedirect("./");
		    }

}
